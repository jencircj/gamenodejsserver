
import express from 'express';
import { app } from './consts';
import './routes';


app.listen(3000, () => {
  console.log('ES6 application listening on port 3000!');
});

/************************************************
code below is to set the static paths for images
*************************************************/
app.use(express.static('public'));