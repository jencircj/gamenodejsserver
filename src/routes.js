import { app, WebServiceConstants } from './consts';
import express from 'express';
import { gameServerUtils } from './gameServerUtils';

app.get('/', (req, res) => {
  res.send('Hello world node.js es6 app.');
});


app.get('/getOutcome', function (request, response) {

	  //Generate an array of 3 random numbers
    var randomNumbersArray = [gameServerUtils.randomIntFromInterval(WebServiceConstants.MIN_RANGE,WebServiceConstants.MAX_RANGE),
                              gameServerUtils.randomIntFromInterval(WebServiceConstants.MIN_RANGE,WebServiceConstants.MAX_RANGE),
                              gameServerUtils.randomIntFromInterval(WebServiceConstants.MIN_RANGE,WebServiceConstants.MAX_RANGE)];
    
    //Below function checks if bonus is triggered
    var bonus = gameServerUtils.getBonusDetails(randomNumbersArray);
    var isBonusTriggered = bonus.isBonusTriggered;

    //No Bonus condition
    if(!isBonusTriggered) {
       bonus = {};
    }
    
    //Set the values for response object
	  response.writeHead(200, { 'Content-Type': 'application/json',
                       'Access-Control-Allow-Origin' : '*',
                       'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
                     });
    response.write(JSON.stringify({randomNumbers: randomNumbersArray,
                                  outcomeName: gameServerUtils.getCountOfSameNumber(randomNumbersArray),
                                  bonusAvailable: isBonusTriggered,
                                  bonusDetails: bonus,
        							            message: "Success"}));  
    response.end();  
});