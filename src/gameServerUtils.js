import { WebServiceConstants } from './consts';

/* ************************************************************************
SINGLETON HELPER CLASS
************************************************************************ */
let instance = null;

class GameServerUtils {
   
    constructor() {

    	if(!instance) {
              instance = this;
        }
        return instance;
    }
   
   /*******************************************************************
   Name - randomIntFromInterval
   Desc - Helper function returns a random number between min and max value
   arguments - min, max
   returnValue - a random number
   ********************************************************************/
    randomIntFromInterval(min,max) {
    return Math.floor(Math.random()*(max-min+1)+min);
   }

    /*******************************************************************
   Name - getOutcomeTitle
   Desc - Helper function that returns the outcomeTitle's which are big_win, small_win and no_win
   arguments - counterNumberObject
   returnValue - a string (big win, small win or no win)
   ********************************************************************/
   getOutcomeTitle(countedNumberObject) {
        console.log('countedNumberObject',countedNumberObject);
        var limitsArray = [];
    		for(var key in countedNumberObject) {
    		    var value = countedNumberObject[key];
                limitsArray.push(value);
    		}
           
            console.log('limitsArray ',limitsArray);
            var maxOccurencesOfNumber = Math.max.apply(Math, limitsArray);
            console.log('maxOccurencesOfNumber ',maxOccurencesOfNumber);
    		if(maxOccurencesOfNumber == 3) {
    	      return WebServiceConstants.BIG_WIN;
    		}
    		else if(maxOccurencesOfNumber == 2) {
    		  return WebServiceConstants.SMALL_WIN;
    		}
    		else {
    		  return WebServiceConstants.NO_WIN;
    		}
		
   }
   
   /*******************************************************************
     Name - getCountOfSameNumber
     Desc - Helper function that returns the outcomeTitle's which are big_win, small_win and no_win
     arguments - numberArray
     returnValue - an object consisting of its occurences
     ********************************************************************/
   getCountOfSameNumber(numberArray) {
	    var countedNumbers = numberArray.reduce(function (allNumbers, number) { 
		  
		  if (number in allNumbers) {
		    allNumbers[number]++;
		  }
		  else {
		    allNumbers[number] = 1;
		  }
		  return allNumbers;
		}, {});

		console.log('outcome Name',this.getOutcomeTitle(countedNumbers));
		return this.getOutcomeTitle(countedNumbers);

    }

    /*******************************************************************
     Name - getBonusDetails
     Desc - Helper function that triggers the bonus feature for player
     arguments - numberArray
     returnValue - an object consisting of bonus details
     ********************************************************************/
    getBonusDetails(numberArray) {

        var bonus = {}
        bonus.isBonusTriggered = false;

        if(numberArray.length > 1 && numberArray[1] == 0){
            bonus.isBonusTriggered = true;
            bonus.bonusTitle = WebServiceConstants.MESSAGE_TITLE;
            bonus.bonusMessage = WebServiceConstants.MESSAGE_DESC;
            bonus.bonusPoints = WebServiceConstants.BONUS_POINTS;
        }
       return bonus;
    }

}

export let gameServerUtils = new GameServerUtils();