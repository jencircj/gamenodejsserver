import express from 'express';
const app = express();

export { app };

export class WebServiceConstants {
	static MESSAGE_TITLE = "Cograts Player!";
	static MESSAGE_DESC = "You have earned 1000 Bonus points.";
	static BONUS_POINTS = "1000 points";
	static NO_WIN = 'No Win';
	static SMALL_WIN = 'Small Win';
	static BIG_WIN = 'Big Win';
	static MIN_RANGE = 0;
    static MAX_RANGE = 5;

}