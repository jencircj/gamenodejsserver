"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.WebServiceConstants = exports.app = undefined;

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var app = (0, _express2.default)();

exports.app = app;

var WebServiceConstants = exports.WebServiceConstants = function WebServiceConstants() {
	_classCallCheck(this, WebServiceConstants);
};

WebServiceConstants.MESSAGE_TITLE = "Cograts Player!";
WebServiceConstants.MESSAGE_DESC = "You have earned 1000 Bonus points.";
WebServiceConstants.BONUS_POINTS = "1000 points";
WebServiceConstants.NO_WIN = 'No Win';
WebServiceConstants.SMALL_WIN = 'Small Win';
WebServiceConstants.BIG_WIN = 'Big Win';
WebServiceConstants.MIN_RANGE = 0;
WebServiceConstants.MAX_RANGE = 5;