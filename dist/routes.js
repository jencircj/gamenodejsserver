'use strict';

var _consts = require('./consts');

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _gameServerUtils = require('./gameServerUtils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_consts.app.get('/', function (req, res) {
   res.send('Hello world node.js es6 app.');
});

_consts.app.get('/getOutcome', function (request, response) {

   //Generate an array of 3 random numbers
   var randomNumbersArray = [_gameServerUtils.gameServerUtils.randomIntFromInterval(_consts.WebServiceConstants.MIN_RANGE, _consts.WebServiceConstants.MAX_RANGE), _gameServerUtils.gameServerUtils.randomIntFromInterval(_consts.WebServiceConstants.MIN_RANGE, _consts.WebServiceConstants.MAX_RANGE), _gameServerUtils.gameServerUtils.randomIntFromInterval(_consts.WebServiceConstants.MIN_RANGE, _consts.WebServiceConstants.MAX_RANGE)];

   //Below function checks if bonus is triggered
   var bonus = _gameServerUtils.gameServerUtils.getBonusDetails(randomNumbersArray);
   var isBonusTriggered = bonus.isBonusTriggered;

   //No Bonus condition
   if (!isBonusTriggered) {
      bonus = {};
   }

   //Set the values for response object
   response.writeHead(200, { 'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
   });
   response.write(JSON.stringify({ randomNumbers: randomNumbersArray,
      outcomeName: _gameServerUtils.gameServerUtils.getCountOfSameNumber(randomNumbersArray),
      bonusAvailable: isBonusTriggered,
      bonusDetails: bonus,
      message: "Success" }));
   response.end();
});