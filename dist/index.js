'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _consts = require('./consts');

require('./routes');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_consts.app.listen(3000, function () {
  console.log('ES6 application listening on port 3000!');
});

/************************************************
code below is to set the static paths for images
*************************************************/
_consts.app.use(_express2.default.static('public'));