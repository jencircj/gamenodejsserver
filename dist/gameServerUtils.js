'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gameServerUtils = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _consts = require('./consts');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* ************************************************************************
SINGLETON HELPER CLASS
************************************************************************ */
var instance = null;

var GameServerUtils = function () {
  function GameServerUtils() {
    _classCallCheck(this, GameServerUtils);

    if (!instance) {
      instance = this;
    }
    return instance;
  }

  /*******************************************************************
  Name - randomIntFromInterval
  Desc - Helper function returns a random number between min and max value
  arguments - min, max
  returnValue - a random number
  ********************************************************************/


  _createClass(GameServerUtils, [{
    key: 'randomIntFromInterval',
    value: function randomIntFromInterval(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /*******************************************************************
    Name - getOutcomeTitle
    Desc - Helper function that returns the outcomeTitle's which are big_win, small_win and no_win
    arguments - counterNumberObject
    returnValue - a string (big win, small win or no win)
    ********************************************************************/

  }, {
    key: 'getOutcomeTitle',
    value: function getOutcomeTitle(countedNumberObject) {
      console.log('countedNumberObject', countedNumberObject);
      var limitsArray = [];
      for (var key in countedNumberObject) {
        var value = countedNumberObject[key];
        limitsArray.push(value);
      }

      console.log('limitsArray ', limitsArray);
      var maxOccurencesOfNumber = Math.max.apply(Math, limitsArray);
      console.log('maxOccurencesOfNumber ', maxOccurencesOfNumber);
      if (maxOccurencesOfNumber == 3) {
        return _consts.WebServiceConstants.BIG_WIN;
      } else if (maxOccurencesOfNumber == 2) {
        return _consts.WebServiceConstants.SMALL_WIN;
      } else {
        return _consts.WebServiceConstants.NO_WIN;
      }
    }

    /*******************************************************************
      Name - getCountOfSameNumber
      Desc - Helper function that returns the outcomeTitle's which are big_win, small_win and no_win
      arguments - numberArray
      returnValue - an object consisting of its occurences
      ********************************************************************/

  }, {
    key: 'getCountOfSameNumber',
    value: function getCountOfSameNumber(numberArray) {
      var countedNumbers = numberArray.reduce(function (allNumbers, number) {

        if (number in allNumbers) {
          allNumbers[number]++;
        } else {
          allNumbers[number] = 1;
        }
        return allNumbers;
      }, {});

      console.log('outcome Name', this.getOutcomeTitle(countedNumbers));
      return this.getOutcomeTitle(countedNumbers);
    }
  }, {
    key: 'getBonusDetails',
    value: function getBonusDetails(numberArray) {

      var bonus = {};
      bonus.isBonusTriggered = false;

      if (numberArray.length > 1 && numberArray[1] == 0) {
        bonus.isBonusTriggered = true;
        bonus.bonusTitle = _consts.WebServiceConstants.MESSAGE_TITLE;
        bonus.bonusMessage = _consts.WebServiceConstants.MESSAGE_DESC;
        bonus.bonusPoints = _consts.WebServiceConstants.BONUS_POINTS;
      }
      return bonus;
    }
  }]);

  return GameServerUtils;
}();

var gameServerUtils = exports.gameServerUtils = new GameServerUtils();